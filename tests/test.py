from grubbs import MIN_OBS, grubbs, detect_outliers

import numpy as np

import unittest

np.random.seed(123)

def make_y(outlier=5, size=20):
    y = np.zeros(shape=(size,))
    return np.insert(y, 0, outlier)

class Test(unittest.TestCase):
    def test_two_sided_false_null(self):
        y = make_y()
        res = detect_outliers(y)
        self.assertEqual(len(res), 1)
        res = res[0]
        self.assertEqual(res['index'], 0)
        self.assertEqual(res['statistic'], (y[0]-y.mean())/y.std(ddof=1))
        self.assertLess(res['pvalue'], .05)

    def test_two_sided_true_null(self):
        y = np.random.normal(size=100)
        res = grubbs(y)
        self.assertGreater(res['pvalue'], .05)

    def test_min(self):
        y = make_y(outlier=-5)
        res = detect_outliers(y, alternative='min')
        self.assertEqual(len(res), 1)
        res = res[0]
        self.assertEqual(res['index'], 0)
        self.assertEqual(res['statistic'], (y.mean()-y[0])/y.std(ddof=1))
        self.assertLess(res['pvalue'], .05)

    def test_max(self):
        y = make_y(outlier=5)
        res = detect_outliers(y, alternative='max')
        self.assertEqual(len(res), 1)
        res = res[0]
        self.assertEqual(res['index'], 0)
        self.assertEqual(res['statistic'], (y[0]-y.mean())/y.std(ddof=1))
        self.assertLess(res['pvalue'], .05)

    def test_valid_alternative(self):
        with self.assertRaises(ValueError):
            grubbs(make_y(), alternative='invalid_alternative')

    def test_min_obs(self):
        with self.assertWarns(RuntimeWarning):
            grubbs(make_y(size=MIN_OBS-2))

if __name__ == '__main__':
    unittest.main()