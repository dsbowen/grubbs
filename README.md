# Grubbs test for outliers

[![pipeline status](https://gitlab.com/dsbowen/example-package/badges/master/pipeline.svg)](https://gitlab.com/dsbowen/grubbs/-/commits/master)
[![coverage report](https://gitlab.com/dsbowen/example-package/badges/master/coverage.svg)](https://gitlab.com/dsbowen/grubbs/-/commits/master)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![PyPI version](https://badge.fury.io/py/grubbs.svg)](https://badge.fury.io/py/grubbs)
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/dsbowen%2Fgrubbs/HEAD)

This package implements the Grubbs test for outliers. See [https://dsbowen.gitlab.io/grubbs/](https://dsbowen.gitlab.io/grubbs/).
