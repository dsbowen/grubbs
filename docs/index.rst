.. Example project documentation master file, created by
   sphinx-quickstart on Mon Mar 22 14:17:44 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Grubbs test
===========

.. image:: https://gitlab.com/dsbowen/grubbs/badges/master/pipeline.svg
   :target: https://gitlab.com/dsbowen/grubbs/-/commits/master

.. image:: https://gitlab.com/dsbowen/grubbs/badges/master/coverage.svg
   :target: https://gitlab.com/dsbowen/grubbs/-/commits/master

.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
   :target: https://github.com/psf/black

.. image:: https://badge.fury.io/py/grubbs.svg
   :target: https://badge.fury.io/py/grubbs

.. image:: https://mybinder.org/badge_logo.svg
   :target: https://mybinder.org/v2/gl/dsbowen%2Fgrubbs/HEAD

Install
-------

.. code-block:: bash

   $ pip install grubbs

.. toctree::
   :maxdepth: 2
   :caption: Contents

   API <api>

License
-------

MIT

Citations
---------

.. code-block:: bibtex

   @article{grubbs1950sample,
      title={Sample criteria for testing outlying observations},
      author={Grubbs, Frank E and others},
      journal={Annals of mathematical statistics},
      volume={21},
      number={1},
      pages={27--58},
      year={1950},
      publisher={Institute of Mathematical Statistics}
   }

   @software{bowen2021grubbs,
      title={Grubbs test for outliers, python implementation},
      author={Dillon Bowen},
      year={2021}
   }
