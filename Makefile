REPORTS_DIR ?= reports
SPHINX_SOURCE_DIR ?= docs
SPHINX_BUILD_DIR ?= docs/_build
SRC_DIR ?= grubbs

.PHONY: format
format:
	black ${SRC_DIR}

.PHONY: test
test:
	coverage run -m unittest discover -s tests
	coverage html -d ${REPORTS_DIR}/coverage
	coverage report

.PHONY: lint
lint:
	mkdir -p ${REPORTS_DIR}
	pylint ${SRC_DIR} --reports=y > ${REPORTS_DIR}/lint.txt

.PHONY: typehint
typehint:
	mkdir -p ${REPORTS_DIR}
	mypy ${SRC_DIR} > ${REPORTS_DIR}/typehint.txt

.PHONY: docmake
docmake:
	python setup.py build_sphinx\
		--source ${SPHINX_SOURCE_DIR}\
		--build-dir ${SPHINX_BUILD_DIR}
.PHONY: doctest
doctest:
	sphinx-build ${SPHINX_SOURCE_DIR} ${REPORTS_DIR} -b doctest
.PHONY: docs
docs: docmake doctest

.PHONY: pipeline
pipeline: format test lint typehint docs
